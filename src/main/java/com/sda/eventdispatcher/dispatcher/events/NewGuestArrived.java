package com.sda.eventdispatcher.dispatcher.events;

import com.sda.eventdispatcher.dispatcher.EventDispatcher;
import com.sda.eventdispatcher.dispatcher.IEvent;
import com.sda.eventdispatcher.listeners.NewGuestArrivedListener;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * Created by amen on 12/4/17.
 */
public class NewGuestArrived implements IEvent {
    private String guestName;

    public NewGuestArrived(String guestName) {
        this.guestName = guestName;
    }

    @Override
    public void run() {
        Optional<List> objectList = EventDispatcher.getInstance().getAllObjectsImplementingInterface(NewGuestArrivedListener.class);

        objectList.ifPresent(new Consumer<List>() {
            @Override
            public void accept(List list) {
                List<NewGuestArrivedListener> listeners = (List<NewGuestArrivedListener>) objectList.get();
                for (NewGuestArrivedListener listener: listeners) {
                    listener.newGuestArrived(guestName);
                }
            }
        });

//        objectList.ifPresent(list -> {
//            List<NewGuestArrivedListener> listeners = (List<NewGuestArrivedListener>) objectList.get();
//            for (NewGuestArrivedListener listener: listeners) {
//                listener.newGuestArrived();
//            }
//        });
        // optional - klasa która przechowuje wartość ale może być null'em
    }
}
