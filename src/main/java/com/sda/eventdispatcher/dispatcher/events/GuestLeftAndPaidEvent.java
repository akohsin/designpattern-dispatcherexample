package com.sda.eventdispatcher.dispatcher.events;

import com.sda.eventdispatcher.dispatcher.EventDispatcher;
import com.sda.eventdispatcher.dispatcher.IEvent;
import com.sda.eventdispatcher.listeners.GuestLeftListener;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * Created by amen on 12/4/17.
 */
public class GuestLeftAndPaidEvent implements IEvent{
    private String guestName;
    private int paidMoney;

    public GuestLeftAndPaidEvent(String guestName, int paidMoney) {
        this.guestName = guestName;
        this.paidMoney = paidMoney;
    }

    @Override
    public void run() {
        Optional<List> objectList = EventDispatcher.getInstance().getAllObjectsImplementingInterface(GuestLeftListener.class);

        objectList.ifPresent(new Consumer<List>() {
            @Override
            public void accept(List list) {
                List<GuestLeftListener> listeners = (List<GuestLeftListener>) objectList.get();
                for (GuestLeftListener listener : listeners) {
                    listener.guestLeft(guestName, paidMoney);
                }
            }
        });
    }
}
