package com.sda.eventdispatcher.restaurant;

import com.sda.eventdispatcher.dispatcher.EventDispatcher;
import com.sda.eventdispatcher.listeners.GuestLeftListener;
import com.sda.eventdispatcher.listeners.GuestPaidListener;
import com.sda.eventdispatcher.listeners.NewGuestArrivedListener;
import java.util.List;

import java.util.ArrayList;

/**
 * Created by amen on 12/4/17.
 */
public class SecurityGuy implements NewGuestArrivedListener, GuestPaidListener, GuestLeftListener {

    private List<String> unpaidGuests = new ArrayList<>();

    public SecurityGuy() {
        EventDispatcher.getInstance().register(this);
    }

    @Override
    public void newGuestArrived(String guestName) {
        unpaidGuests.add(guestName);
    }

    @Override
    public void guestPaid(String guestName) {
        unpaidGuests.remove(guestName);
    }

    @Override
    public void guestLeft(String guestName) {
        if(unpaidGuests.contains(guestName)){
            System.out.println(guestName + " ucieka bez placenia");
        }
    }

    @Override
    public void guestLeft(String guestName, int paidMoney) {
        if(unpaidGuests.contains(guestName)){
            System.out.println(guestName + " placi "+ paidMoney+" i wychodzi");
            unpaidGuests.remove(guestName);
        }
    }
}
