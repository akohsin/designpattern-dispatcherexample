package com.sda.eventdispatcher.listeners;

import com.sda.eventdispatcher.restaurant.Order;

/**
 * Created by amen on 12/4/17.
 */
public interface OrderListener {
    void newOrder(Order what);
}
