package com.sda.eventdispatcher.listeners;

/**
 * Created by amen on 12/4/17.
 */
public interface GuestLeftListener {
    void guestLeft(String guestName);
    void guestLeft(String guestName, int paidMoney);
}
