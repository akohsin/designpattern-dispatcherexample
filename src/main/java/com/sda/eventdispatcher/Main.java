package com.sda.eventdispatcher;

import com.sda.eventdispatcher.dispatcher.EventDispatcher;
import com.sda.eventdispatcher.dispatcher.events.*;
import com.sda.eventdispatcher.restaurant.Order;
import com.sda.eventdispatcher.restaurant.Restaurant;

import java.util.Scanner;

/**
 * Created by amen on 12/4/17.
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        Restaurant restaurant = new Restaurant();

        boolean isWorking = true;
        while(isWorking){
            String line = sc.nextLine().trim().toLowerCase();

            if(line.startsWith("order ")){
                String what = line.replace("order ", "");
                EventDispatcher.getInstance().fireEvent(new NewOrderEvent(new Order(what)));
            }else if (line.startsWith("waiter ")){
                String waiterName = line.replace("waiter ", "");
                restaurant.addWaiter(waiterName);
            }else if(line.startsWith("guest ")){
                String guestName = line.replace("guest ", "");
                EventDispatcher.getInstance().fireEvent(new NewGuestArrived(guestName));
            }else if(line.startsWith("leave ")){
                String guestName = line.replace("leave ", "");
                EventDispatcher.getInstance().fireEvent(new GuestLeftEvent(guestName));
            }else if(line.startsWith("leavenpay ")){
                String guestName = line.replace("leavenpay ", "");
                EventDispatcher.getInstance().fireEvent(new GuestLeftAndPaidEvent(guestName, 20));
            }else if(line.startsWith("guestpaid ")){
                String guestName = line.replace("guestpaid ", "");
                EventDispatcher.getInstance().fireEvent(new GuestPaidEvent(guestName));
            }else if(line.startsWith("quit")){
                break;
            }
        }
    }
}
